<%@ page contentType="text/html;charset=UTF-8" %>
%{--
<g:set var="grailsResourceProcessor" bean="grailsResourceProcessor"/>
--}%
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Resources Debug</title>
    <style type="text/css" media="screen">
    #page-body {
        margin: 2em 1em 1.25em;
    }
    h1 {
        font-size: 1.5em;
    }

    h2 {
        margin-top: 1em;
        margin-bottom: 0.3em;
    }

    p {
        line-height: 1.5;
        margin: 0.25em 0;
    }
    ul {
        margin: 5px 15px;
    }

    @media screen and (max-width: 480px) {
        #status {
            display: none;
        }

        #page-body {
            margin: 0 1em 1em;
        }

        #page-body h1 {
            margin-top: 0;
        }
    }
    </style>

</head>
<body>
<div id="page-body" role="main">
    <h1>Modules</h1>
    <g:each in="${grailsResourceProcessor.modulesInDependencyOrder}" var="moduleName">
        <g:set var="module" value="${grailsResourceProcessor.getModule(moduleName)}"/>
        <g:if test="${module}">
            <h2>${module.name}</h2>
            <a name="module_${module.name}"></a>
            <p>
                <g:if test="${module.dependsOn}">
                    <em>Depends on:</em>
                    <g:each in="${module.dependsOn}" var="dependency">
                        <a href="#module_${dependency}">${dependency}</a>
                    </g:each>
                </g:if>
                <g:if test="${module.resources}">

                    <g:each in="${module.resources}" var="resource">
                        <h3>${resource.id?:resource.originalUrl}</h3>
                        <ul>
                            <li><em>Working dir:</em> ${resource.workDir}</li>
                            <g:if test="${resource.originalResource}">
                                <li><em>Original file:</em> ${resource.originalResource.URL}</li>
                            </g:if>
                            <g:if test="${resource.processedFile}">
                                <li><em>Processed file:</em> ${resource.processedFile}</li>
                            </g:if>
                            <li><em>Original URL:</em> <a href="${request.contextPath}${resource.originalUrl}" target="_blank">${resource.originalUrl}</a></li>
                            <g:if test="${resource.sourceUrl != resource.originalUrl}">
                                <li><em>Source URL(s):</em> ${resource.sourceUrl}</li>

                            </g:if>
                            <g:if test="${resource.actualUrl != resource.originalUrl}">
                                <li><em>Actual URL:</em> <a href="${request.contextPath}/static${resource.actualUrl}" target="_blank">/static${resource.actualUrl}</a></li>
                            </g:if>
                            <li><em>Content type:</em> ${resource.contentType}</li>
                            <li><em>Disposition:</em> ${resource.disposition}</li>
                            <li><em>Attributes:</em> ${resource.attributes}</li>
                            <g:if test="${resource.tagAttributes}">
                                <li><em>Tag attributes:</em> ${resource.tagAttributes}</li>
                            </g:if>
                            <li><em>Original size:</em> <g:formatNumber number="${resource.originalContentLength / 1024}" maxFractionDigits="2"></g:formatNumber>  kb</li>
                            <li><em>Actual size:</em> <g:formatNumber number="${resource.contentLength / 1024}" maxFractionDigits="2"></g:formatNumber>  kb</li>
                            <li><em>Exists?</em> ${resource.exists()}</li>
                        </ul>



                    </g:each>

                </g:if>
            </p>
        </g:if>
    </g:each>
</div>

</body>
</html>