package org.grails.plugin.resources.debug

class ResourcesDebugController {

    def grailsResourceProcessor

    def index() {
        [grailsResourceProcessor: grailsResourceProcessor]
    }
}
