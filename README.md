Have you ever found yourself debugging Grails static resources? Yeah, mee too. I'm sorry for that.

Debugging module definition, Javascript minifying and so on is an unpleasant task given the options you usually have:
you either turn on log debugging for resources plugin, which throws away **millions** of lines) or inspect the generated
HTML, trying to find clues of how your resources were rendered.

This simple plugin will not eliminate the need of debugging, but it will help you in case you need to do it. It provides
a simple controller which will render all the modules, resources and dependencies loaded by the Resources plugin. Not only
from your `ApplicationResources.groovy`, but also from other resource providers plugins you may have installed.

Note that this plugin is intentionally excluded from production environment and for WAR generation.

Once you have it installed, then go to `/resourcesDebug` to get an output like this:

![Screenshot](https://bitbucket.org/alvaro_sanchez/grails-resources-debug/raw/master/screenshot.png)