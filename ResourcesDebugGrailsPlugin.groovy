class ResourcesDebugGrailsPlugin {
    // the plugin version
    def version = "1.0"
    // the version or versions of Grails the plugin is designed for
    def grailsVersion = "2.0 > *"

    def dependsOn = [resources: '1.2 > *']

    def environments = [excludes: ["production"]]
    def scopes = [excludes:'war']


    // TODO Fill in these fields
    def title = "Resources Debug Plugin" // Headline display name of the plugin
    def author = "Álvaro Sánchez-Mariscal"
    def authorEmail = "alvaro.sanchez@salenda.es"
    def description = '''\
Simple plugin to help debugging the runtime modules loaded by the resources plugin.'''

    // URL to the plugin's documentation
    def documentation = "https://bitbucket.org/alvaro_sanchez/grails-resources-debug"

    // Extra (optional) plugin metadata

    // License: one of 'APACHE', 'GPL2', 'GPL3'
    def license = "APACHE"

    // Location of the plugin's issue tracker.
    def issueManagement = [ system: "Bitbucket", url: "https://bitbucket.org/alvaro_sanchez/grails-resources-debug/issues" ]

    // Online location of the plugin's browseable source code.
    def scm = [ url: "https://bitbucket.org/alvaro_sanchez/grails-resources-debug" ]

}
